<?xml version="1.0" encoding="UTF-8"?>

<frontmatter xmlns:xi="http://www.w3.org/2001/XInclude" xml:id="aata" permid="Wih">
  <titlepage>
    <author>
      <personname>Georg Loho</personname>
       <institution>University of Twente</institution>
      <email>g.loho@utwente.nl</email>
    </author>

    <author>
      <personname>Thomas W. Judson</personname>
      <department>Department of Mathematics and Statistics</department>
      <institution>Stephen F. Austin State University</institution>
      <email>judsontw@sfasu.edu</email>
    </author>
    
    <date>
      <today/>
    </date>
    </titlepage>
    <colophon>

      <edition>2023</edition>
      <website>
	<name>
	  <c>gitlab.utwente.nl/m7700065/applied-algebra-am-tcs-2023</c>
      </name>
      <address>https://gitlab.utwente.nl/m7700065/applied-algebra-am-tcs-2023</address>
    </website>
      
      <copyright>
      <year>1997<ndash/>2022</year>
      <holder>Thomas W. Judson, Robert A. Beezer</holder>
      <minilicense>GFDL License</minilicense>
      <shortlicense>
        Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included in the appendix entitled <q>GNU Free Documentation License.</q>
      </shortlicense>

    </copyright>
      
      <copyright>
	<year>2023</year>
	<holder>Georg Loho</holder>
	<minilicense>GFDL License</minilicense>
	<shortlicense>
	  Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included in the appendix entitled <q>GNU Free Documentation License.</q>
	</shortlicense>
      </copyright>
    </colophon>
    
    <acknowledgement>
      <p>
	I would like to acknowledge the following people in giving helpful insights and visualizations for the modified version.
	<ul>
	  <li>
	    Stefano Piceghello, University of Twente
	  </li>
	</ul>
      </p>
      <p>
	<em>The following acknowledgements are written by Thomas W. Judson for the original version of the book. </em>
      </p>
    <p permid="ewA">
      I would like to acknowledge the following reviewers for their helpful comments and suggestions.

      <ul permid="ODI">
        <li permid="uKR">
          <p permid="EeD">
            David Anderson, University of Tennessee, Knoxville
          </p>
        </li>

        <li permid="aSa">
          <p permid="klM">
            Robert Beezer, University of Puget Sound
          </p>
        </li>

        <li permid="GZj">
          <p permid="QsV">
            Myron Hood, California Polytechnic State University
          </p>
        </li>

        <li permid="ngs">
          <p permid="wAe">
            Herbert Kasube, Bradley University
          </p>
        </li>

        <li permid="TnB">
          <p permid="cHn">
            John Kurtzke, University of Portland
          </p>
        </li>

        <li permid="zuK">
          <p permid="IOw">
            Inessa Levi, University of Louisville
          </p>
        </li>

        <li permid="fBT">
          <p permid="oVF">
            Geoffrey Mason, University of California, Santa Cruz
          </p>
        </li>

        <li permid="LJc">
          <p permid="VcO">
            Bruce Mericle, Mankato State University
          </p>
        </li>

        <li permid="rQl">
          <p permid="BjX">
            Kimmo Rosenthal, Union College
          </p>
        </li>

        <li permid="XXu">
          <p permid="hrg">
            Mark Teply, University of Wisconsin
          </p>
        </li>
      </ul>
    </p>

    <p permid="KDJ">
      I would also like to thank Steve Quigley, Marnie Pommett, Cathie Griffin, Kelle Karshick,
      and the rest of the staff at PWS Publishing for their guidance throughout this project.
      It has been a pleasure to work with them.
    </p>

    <p permid="qKS">
      Robert Beezer encouraged me to make
      <em>Abstract Algebra: Theory and Applications</em>
      available as an open source textbook,
      a decision that I have never regretted.
      With his assistance, the book has been rewritten in PreTeXt
      (<url href="https://pretextbook.org" visual="pretextbook.org"><c>pretextbook.org</c></url>),
      making it possible to quickly output print, web,
      <acro>PDF</acro> versions and more from the same source.
      The open source version of this book has received support from the National Science Foundation (Awards #DUE-1020957, #DUE–1625223, and #DUE–1821329).
    </p>
  </acknowledgement>
    
    <preface>
      <title>History </title>
      
      <p>
	Original Version
      </p>
      <p>
	<ul>
	  <li>
	    Title: Abstract Algebra, Theory and Applications
	  </li>
	  <li>
	    Year of version: 2022
	  </li>
	  <li>
	    Original author: Thomas W. Judson
	  </li>
	  <li>
	    <url href="https://judsonbooks.org/aata/">judsonbooks.org/aata</url>
	  </li>
	</ul>
      </p>
      
      <p>
	Modified Version
      </p>
      <p>
	<ul>
	  <li>
	    Title: Abstract Algebra for AM and TCS - A short introduction with applications 
	  </li>
	  <li>
	    Year: 2023
	  </li>
	  <li>
	    New authors: Georg Loho
	  </li>
	  <li>
	    <url href="https://gitlab.utwente.nl/m7700065/applied-algebra-am-tcs-2023">gitlab.utwente.nl/m7700065/applied-algebra-am-tcs-2023</url>
	  </li>
	</ul>
      </p>
    </preface>

    <preface>
      <title>How to (read this book)</title>
      <p>
	This text is a compact introduction to Abstract Algebra in 10 lectures.
	It is tailored to a course with students from Applied Mathematics and Technical Computer Science. 
	Each chapter corresponds to one lecture; it starts with a list of basic learning goals.
	The "Additional insights" in each chapter go beyond the core scope but are interesting additions. 
      </p>
      <p>
	Towards the end of each chapter, there are sections "Core Exercises" and "Additional Exercises".
	The exercises in the section "Core Exercises" are important for the understanding of the course.
	The "Additional Exercises" are just a collection for further study. 
      </p>
      <p>
	Furthermore, towards the end of each chapter, there is the section "Material".
	Most importantly, this section contains links to videos which might provide additional help for understanding the content of the course.
	These videos cover most of the core topics but not everything; they are not maintained by us but are well-selected from other media sources. 
      </p>
      <p>
	Additionally, at the end of each chapter, there is a section with hints for selected exercises. 
	Finally, there is a chapter "Comments to Core Exercises" in the Appendix.
	This is meant to be the last resort, if one has not been able to solve the exercises after weeks of trying.
	</p>


    </preface>

</frontmatter>

