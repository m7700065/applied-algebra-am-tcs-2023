# Applied Algebra AM-TCS 2023

by Georg Loho
-------------

This repository contains the sources files in [PreTeXt](https://pretextbook.org/) format for an open source [GFDL](https://www.gnu.org/copyleft/fdl.html)-licensed reader on basics of algebra with applications; it is based on the book [judsonbooks.org/aata](https://judsonbooks.org/aata/).

## Content
This text is a compact introduction to Abstract Algebra in 10 lectures.
It is tailored to a course with students from Applied Mathematics and Technical Computer Science, specifically those in the Math Module 5 of the University of Twente. 
Each chapter corresponds to one lecture. 

## Usage
The sources are provided so that the course can be further adapted to the specific needs in a given study curriculum. 

## Copyright

Copyright (C) 1997-2022 Thomas W. Judson, Robert A. Beezer.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

Copyright (C) 2023 Georg Loho
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

